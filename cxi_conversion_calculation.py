import sys
sys.path.insert(1, '../../')
import numpy as np
import matplotlib as matplotlib
import matplotlib.pyplot as plt
import copy
from scipy import interpolate

sys.path.insert(1, '../../../')
sys.path.insert(1, '/Users/maxmajor/Github/pyfidasim')
sys.path.insert(1, '/Users/maxmajor/Github/pystrahl2')
sys.path.insert(1, '/Users/maxmajor/Github/pystrahl2/pystrahl')
sys.path.insert(1, '/Users/maxmajor/Github/pystrahl2/examples/DIII_D')
sys.path.insert(1, '/Users/maxmajor/Github/pystrahl2/examples/DIII_D/TRANSP')
sys.path.insert(1, '/Users/maxmajor/Github/pyfidasim/examples/DIIID/Data/')
sys.path.insert(1, '/Users/maxmajor/Github/pyfidasim/examples/DIIID/')


def get_qeff_intensity(einj, dens, ti, zeff, impdens, beam_dens):
    q_eff = 0  ## for full, half, one thrid and halo neutrals
    impurity_intensity = 0
    dl = 0.1
    # q_eff[1, 0] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
    #                                       ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
    #                                       [einj / 1., dens, ti, zeff])
    # q_eff[1, 1] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
    #                                       ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
    #                                       [einj / 2., dens, ti, zeff])
    # q_eff[1, 2] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
    #                                       ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
    #                                       [einj / 3., dens, ti, zeff])
    # q_eff[1, 3] = get_from_tabulated_data(qef_therm_file, 'n_1_emission_coefficient',
    #                                       ['electron_density', 'ion_temperature', 'z_effective'],
    #                                       [dens_default, ti, zeff])
    # impurity_intensity[1, :] += beam_dens * q_eff[1, :] * impdens * dl  # ph/s/cm^2

    ##------------------------------
    ## n=2 state
    ##------------------------------
    # q_eff = np.zeros(4)  ## for full, half, one thrid and halo neutrals
    q_eff = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                          [einj / 1., dens, ti, zeff])
    impurity_intensity += beam_dens * q_eff * impdens * dl  # ph/s/cm^2
    return q_eff, impurity_intensity






from pyfidasim.tables.get_cxrs_rates_netcdf import get_from_tabulated_data
directory = '/Users/maxmajor/Github/pyfidasim/pyfidasim/tables/'
qef_beam_file=directory+'qef_DD_C5_beam'
qef_therm_file = directory + 'qef_D_C5_therm'
##------------------------------
## n=1 state
##------------------------------
einj_default = 38004.8
dens_default = 2.28e13
ti_default = 1283.9
zeff_default = 1.945

einj_list = [14, 18, 22, 24, 26, 28, 30, 34, 36, 38, 40, 42,
             45, 47, 50, 51, 52, 53, 55, 57, 60]
for i in range(len(einj_list)):
    einj_list[i] *= 1000
#print(einj_list)
dens_list = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
             1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2.0, 2.1, 2.2,
             2.28, 2.4, 2.5, 2.6, 2.7, 2.9, 3.1, 3.3, 3.5, 3.7]
for i in range(len(dens_list)):
    dens_list[i] *= 1.e13
ti_list = [0.8, 0.84, 0.88, 0.92, 0.96, 1.0, 1.04, 1.08, 1.12, 1.16, 1.2, 1.24,
           1.28, 1.32, 1.36, 1.4, 1.44, 1.48, 1.52, 1.56, 1.6]
for i in range(len(ti_list)):
    ti_list[i] *= 1000
zeff_list = [1.1, 1.2, 1.5, 1.8, 1.945, 2.0, 2.5, 3.0, 3.5, 4.0]

q_eff_einj = []
for einj in einj_list:
    q_eff = np.zeros((2,4))  ## for full, half, one thrid and halo neutrals
    q_eff[1,0] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                       ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                       [einj / 1., dens_default, ti_default, zeff_default])
    q_eff[1,1] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                       ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                       [einj / 2., dens_default, ti_default, zeff_default])
    q_eff[1,2] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                       ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                       [einj / 3., dens_default, ti_default, zeff_default])
    q_eff[1,3] = get_from_tabulated_data(qef_therm_file, 'n_1_emission_coefficient',
                                       ['electron_density', 'ion_temperature', 'z_effective'],
                                       [dens_default, ti_default, zeff_default])

    ##------------------------------
    ## n=2 state
    ##------------------------------
    #q_eff = np.zeros(4)  ## for full, half, one thrid and halo neutrals
    q_eff[0,0] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                       ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                       [einj / 1., dens_default, ti_default, zeff_default])
    q_eff[0,1] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                       ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                       [einj / 2., dens_default, ti_default, zeff_default])
    q_eff[0,2] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                       ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                       [einj / 3., dens_default, ti_default, zeff_default])
    q_eff[0,3] = get_from_tabulated_data(qef_therm_file, 'n_2_emission_coefficient',
                                       ['electron_density', 'ion_temperature', 'z_effective'],
                                       [dens_default, ti_default, zeff_default])
    q_eff_einj.append(q_eff)



qeff_einj_full = []
qeff_einj_full2 = []

qeff_einj_half = []
qeff_einj_half2 = []

qeff_einj_third = []
qeff_einj_third2 = []

qeff_einj_halo = []
qeff_einj_halo2 = []
for i in range(len(einj_list)):
    qeff_einj_full.append(q_eff_einj[i][0][0])
    qeff_einj_full2.append(q_eff_einj[i][1][0])

    qeff_einj_half.append(q_eff_einj[i][0][1])
    qeff_einj_half2.append(q_eff_einj[i][1][1])

    qeff_einj_third.append(q_eff_einj[i][0][2])
    qeff_einj_third2.append(q_eff_einj[i][1][2])

    qeff_einj_halo.append(q_eff_einj[i][0][3])
    qeff_einj_halo2.append(q_eff_einj[i][1][3])






plt.figure()
plt.plot(einj_list, qeff_einj_full, label="Full, n=1")
#plt.plot(einj_list, qeff_einj_half, label="Half, n=1")
#plt.plot(einj_list, qeff_einj_third, label="Third, n=1")
#plt.plot(einj_list, qeff_einj_halo, label="Halo, n=1")

plt.plot(einj_list, qeff_einj_full2, label="Full, n=2")
#plt.plot(einj_list, qeff_einj_half2, label="Half, n=2")
#plt.plot(einj_list, qeff_einj_third2, label="Third, n=2")
#plt.plot(einj_list, qeff_einj_halo2, label="Halo, n=2")

plt.xlabel("Beam Energy [eV]")
plt.ylabel("Emissivity")
plt.title("Emissivity vs. Beam Energy")

plt.legend()




q_eff_dens = []
for dens in dens_list:
    q_eff = np.zeros((2, 4))  ## for full, half, one thrid and halo neutrals
    q_eff[1, 0] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                          ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                          [einj_default / 1., dens, ti_default, zeff_default])
    q_eff[1, 1] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                          ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                          [einj_default / 2., dens, ti_default, zeff_default])
    q_eff[1, 2] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                          ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                          [einj_default / 3., dens, ti_default, zeff_default])
    q_eff[1, 3] = get_from_tabulated_data(qef_therm_file, 'n_1_emission_coefficient',
                                          ['electron_density', 'ion_temperature', 'z_effective'],
                                          [dens, ti_default, zeff_default])

    ##------------------------------
    ## n=2 state
    ##------------------------------
    # q_eff = np.zeros(4)  ## for full, half, one thrid and halo neutrals
    q_eff[0, 0] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                          [einj_default / 1., dens, ti_default, zeff_default])
    q_eff[0, 1] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                          [einj_default / 2., dens, ti_default, zeff_default])
    q_eff[0, 2] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_density', 'ion_temperature', 'z_effective'],
                                          [einj_default / 3., dens, ti_default, zeff_default])
    q_eff[0, 3] = get_from_tabulated_data(qef_therm_file, 'n_2_emission_coefficient',
                                          ['electron_density', 'ion_temperature', 'z_effective'],
                                          [dens, ti_default, zeff_default])
    q_eff_dens.append(q_eff)

qeff_dens_full = []
qeff_dens_full2 = []

qeff_dens_half = []
qeff_dens_half2 = []

qeff_dens_third = []
qeff_dens_third2 = []

qeff_dens_halo = []
qeff_dens_halo2 = []
for i in range(len(dens_list)):
    qeff_dens_full.append(q_eff_dens[i][0][0])
    qeff_dens_full2.append(q_eff_dens[i][1][0])

    qeff_dens_half.append(q_eff_dens[i][0][1])
    qeff_dens_half2.append(q_eff_dens[i][1][1])

    qeff_dens_third.append(q_eff_dens[i][0][2])
    qeff_dens_third2.append(q_eff_dens[i][1][2])

    qeff_dens_halo.append(q_eff_dens[i][0][3])
    qeff_dens_halo2.append(q_eff_dens[i][1][3])
    
plt.figure()
plt.plot(dens_list, qeff_dens_full, label="Full, n=1")
#plt.plot(dens_list, qeff_dens_half, label="Half, n=1")
#plt.plot(dens_list, qeff_dens_third, label="Third, n=1")
#plt.plot(dens_list, qeff_dens_halo, label="Halo, n=1")

plt.plot(dens_list, qeff_dens_full2, label="Full, n=2")
#plt.plot(dens_list, qeff_dens_half2, label="Half, n=2")
#plt.plot(dens_list, qeff_dens_third2, label="Third, n=2")
#plt.plot(dens_list, qeff_dens_halo2, label="Halo, n=2")

plt.xlabel("Electron Density [m^-3]")
plt.ylabel("Emissivity")
plt.title("Emissivity vs. Electron Density")

plt.legend()

q_eff_ti = []
for ti in ti_list:
    q_eff = np.zeros((2, 4))  ## for full, half, one thrid and halo neutrals
    q_eff[1, 0] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 1., dens_default, ti, zeff_default])
    q_eff[1, 1] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 2., dens_default, ti, zeff_default])
    q_eff[1, 2] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 3., dens_default, ti, zeff_default])
    q_eff[1, 3] = get_from_tabulated_data(qef_therm_file, 'n_1_emission_coefficient',
                                          ['electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [dens_default, ti, zeff_default])

    ##------------------------------
    ## n=2 state
    ##------------------------------
    # q_eff = np.zeros(4)  ## for full, half, one thrid and halo neutrals
    q_eff[0, 0] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 1., dens_default, ti, zeff_default])
    q_eff[0, 1] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 2., dens_default, ti, zeff_default])
    q_eff[0, 2] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 3., dens_default, ti, zeff_default])
    q_eff[0, 3] = get_from_tabulated_data(qef_therm_file, 'n_2_emission_coefficient',
                                          ['electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [dens_default, ti, zeff_default])
    q_eff_ti.append(q_eff)

qeff_ti_full = []
qeff_ti_full2 = []

qeff_ti_half = []
qeff_ti_half2 = []

qeff_ti_third = []
qeff_ti_third2 = []

qeff_ti_halo = []
qeff_ti_halo2 = []
for i in range(len(ti_list)):
    qeff_ti_full.append(q_eff_ti[i][0][0])
    qeff_ti_full2.append(q_eff_ti[i][1][0])

    qeff_ti_half.append(q_eff_ti[i][0][1])
    qeff_ti_half2.append(q_eff_ti[i][1][1])

    qeff_ti_third.append(q_eff_ti[i][0][2])
    qeff_ti_third2.append(q_eff_ti[i][1][2])

    qeff_ti_halo.append(q_eff_ti[i][0][3])
    qeff_ti_halo2.append(q_eff_ti[i][1][3])

plt.figure()
plt.plot(ti_list, qeff_ti_full, label="Full, n=1")
# plt.plot(ti_list, qeff_ti_half, label="Half, n=1")
# plt.plot(ti_list, qeff_ti_third, label="Third, n=1")
# plt.plot(ti_list, qeff_ti_halo, label="Halo, n=1")

plt.plot(ti_list, qeff_ti_full2, label="Full, n=2")
# plt.plot(ti_list, qeff_ti_half2, label="Half, n=2")
# plt.plot(ti_list, qeff_ti_third2, label="Third, n=2")
# plt.plot(ti_list, qeff_ti_halo2, label="Halo, n=2")

plt.xlabel("Ion Temperature [eV]")
plt.ylabel("Emissivity")
plt.title("Emissivity vs. Ion Temperature")

plt.legend()



q_eff_zeff = []
for zeff in zeff_list:
    q_eff = np.zeros((2, 4))  ## for full, half, one thrid and halo neutrals
    q_eff[1, 0] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 1., dens_default, ti_default, zeff])
    q_eff[1, 1] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 2., dens_default, ti_default, zeff])
    q_eff[1, 2] = get_from_tabulated_data(qef_beam_file, 'n_1_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 3., dens_default, ti_default, zeff])
    q_eff[1, 3] = get_from_tabulated_data(qef_therm_file, 'n_1_emission_coefficient',
                                          ['electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [dens_default, ti_default, zeff])

    ##------------------------------
    ## n=2 state
    ##------------------------------
    # q_eff = np.zeros(4)  ## for full, half, one thrid and halo neutrals
    q_eff[0, 0] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 1., dens_default, ti_default, zeff])
    q_eff[0, 1] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 2., dens_default, ti_default, zeff])
    q_eff[0, 2] = get_from_tabulated_data(qef_beam_file, 'n_2_emission_coefficient',
                                          ['beam_energy', 'electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [einj_default / 3., dens_default, ti_default, zeff])
    q_eff[0, 3] = get_from_tabulated_data(qef_therm_file, 'n_2_emission_coefficient',
                                          ['electron_dens_defaultity', 'ion_temperature', 'z_effective'],
                                          [dens_default, ti_default, zeff])
    q_eff_zeff.append(q_eff)

qeff_zeff_full = []
qeff_zeff_full2 = []

qeff_zeff_half = []
qeff_zeff_half2 = []

qeff_zeff_third = []
qeff_zeff_third2 = []

qeff_zeff_halo = []
qeff_zeff_halo2 = []
for i in range(len(zeff_list)):
    qeff_zeff_full.append(q_eff_zeff[i][0][0])
    qeff_zeff_full2.append(q_eff_zeff[i][1][0])

    qeff_zeff_half.append(q_eff_zeff[i][0][1])
    qeff_zeff_half2.append(q_eff_zeff[i][1][1])

    qeff_zeff_third.append(q_eff_zeff[i][0][2])
    qeff_zeff_third2.append(q_eff_zeff[i][1][2])

    qeff_zeff_halo.append(q_eff_zeff[i][0][3])
    qeff_zeff_halo2.append(q_eff_zeff[i][1][3])

plt.figure()
plt.plot(zeff_list, qeff_zeff_full, label="Full, n=1")
# plt.plot(zeff_list, qeff_zeff_half, label="Half, n=1")
# plt.plot(zeff_list, qeff_zeff_third, label="Third, n=1")
# plt.plot(zeff_list, qeff_zeff_halo, label="Halo, n=1")

plt.plot(zeff_list, qeff_zeff_full2, label="Full, n=2")
# plt.plot(zeff_list, qeff_zeff_half2, label="Half, n=2")
# plt.plot(zeff_list, qeff_zeff_third2, label="Third, n=2")
# plt.plot(zeff_list, qeff_zeff_halo2, label="Halo, n=2")

plt.xlabel("Z-effective")
plt.ylabel("Emissivity")
plt.title("Emissivity vs. Z-effective")

plt.legend()

impdens_default = 7.21e11 #CHANGE THESE LATER
beam_dens_default = 8.25e8


q_eff_base, impurity_intensity_base = get_qeff_intensity(einj_default, dens_default, ti_default,
                                                         zeff_default,
                                                         impdens_default, beam_dens_default)

q_eff_list_ne = []
impurity_intensity_list_ne = []

q_eff_list_imp = []
impurity_intensity_list_imp = []

scale_list = np.linspace(-0.2, 0.2, 41) #Scaling either carbon or electron density by parameter between -20 and +20%

dens_scale_list = [] #Electron density
imp_scale_list = [] #Impurity density

for i in range(len(scale_list)):
    impdens_scale = impdens_default*(1+scale_list[i])
    imp_scale_list.append(impdens_scale)
    dens_scale = dens_default*(1+scale_list[i])
    dens_scale_list.append(dens_scale)

    q_eff_imp, impurity_intensity_imp = get_qeff_intensity(einj_default, dens_default, ti_default, zeff_default,
                                                           impdens_scale, beam_dens_default)
    q_eff_ne, impurity_intensity_ne = get_qeff_intensity(einj_default, dens_scale, ti_default, zeff_default,
                                                           impdens_default, beam_dens_default)
    q_eff_list_ne.append(q_eff_ne)
    q_eff_list_imp.append(q_eff_imp)

    impurity_intensity_list_ne.append(impurity_intensity_ne)
    impurity_intensity_list_imp.append(impurity_intensity_imp)

qeff_full_ne = []
qeff_full_imp = []
impurity_intensity_full_ne = []
impurity_intensity_full_imp = []
#print(q_eff_list_ne)
for i in range(len(scale_list)):
    qeff_full_ne.append(q_eff_list_ne[i])
    qeff_full_imp.append(q_eff_list_imp[i])

    impurity_intensity_full_ne.append(impurity_intensity_list_ne[i])
    impurity_intensity_full_imp.append(impurity_intensity_list_imp[i])


plt.figure()
plt.plot(dens_scale_list, qeff_full_ne)
plt.title("Electron Density vs. Emissivity")

plt.figure()
plt.plot(imp_scale_list, qeff_full_imp)
plt.title("Carbon Density vs. Emissivity")


plt.figure()
plt.plot(dens_scale_list, impurity_intensity_full_ne)
plt.title("Electron Density vs. Intensity")

plt.figure()
plt.plot(imp_scale_list, impurity_intensity_full_imp)
plt.title("Carbon Density vs. Intensity")


impdens_frac = []
ne_frac = []

intensity_ne_frac = []
intensity_imp_frac = []
for i in range(len(scale_list)):
    impdens_frac.append(impdens_default*scale_list[i])
    ne_frac.append(dens_default*scale_list[i])

    intensity_ne_frac.append(impurity_intensity_full_ne[i]-impurity_intensity_base)
    intensity_imp_frac.append(impurity_intensity_full_imp[i]-impurity_intensity_base)

plt.figure()
plt.plot(ne_frac, intensity_ne_frac)
plt.title("dI vs. dn_e")

plt.figure()
plt.plot(impdens_frac, intensity_imp_frac)
plt.title("dI vs. dn_c")

I_norm = []
n_norm = []
conversion_factor = []
print("Electron density list: ", dens_list)
ne_scale = np.linspace(min(dens_list), max(dens_list), len(intensity_imp_frac))
for i in range(len(intensity_imp_frac)):
    I_norm.append(intensity_imp_frac[i]/impurity_intensity_full_imp[i])
    n_norm.append(impdens_frac[i]/imp_scale_list[i])
    conversion_factor.append(I_norm[i]/n_norm[i])

plt.figure()
plt.plot(dens_scale_list, conversion_factor)
plt.xlabel("Electron Density")
plt.ylabel(r"$(dI/I)/(dn_c/n_c)$")
print(I_norm)
print(n_norm)
print(intensity_imp_frac)
print(impurity_intensity_full_imp)
print(conversion_factor)
plt.ylim(0,1.2)

plt.show()